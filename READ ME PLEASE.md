<div id="header" align="center">
  <img src="https://media.giphy.com/media/dWesBcTLavkZuG35MI/giphy.gif" width="320"/>
</div> 

<div id="badges" align="center">
  <a href="https://www.linkedin.com/in/alexander-novikov-102639135">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
  </a>
  <a href="http://www.khanacademy.org/profile/Trieg01">
    <img src="https://img.shields.io/badge/KhanAcademy-%2314BF96.svg?style=for-the-badge&logo=KhanAcademy&logoColor=white" alt="Khan Academy Badge"/>
  </a>
  <a href="https://www.codewars.com/users/Trieg">
    <img src="https://img.shields.io/badge/Codewars-B1361E?style=for-the-badge&logo=codewars&logoColor=grey)" alt="Codewars Badge"/>
  </a>
  <a href="https://t.me/Trieg01">
    <img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white" alt="Telegram Badge"/>
  </a>
</div>
<div id="counter" align="center">
    <img src="https://komarev.com/ghpvc/?username=your-github-username&style=flat-square&color=blue" alt="counter"/>
<div>

---
<div id="bio" align="left">

### About me 
I'm data analyst from Moscow, Russia. Studied psychology in university, data exploration at Karpov's course and now I'm about to become a student in MIPT


### In the repository

In this repository I collect code I've written and often use in my projects. For instance I participate in a few data analyst hackatons such as  Severstal x McKinsey and Vkusvill Stores. All data I contain here was used for a perfect work.

For sure it might show you my experience in case you get the repository after getting my CV. 

### :hammer_and_wrench: Some of languages and tools I use at work:
<div>
<img src="https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54"/>
<img src="https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white"/>
<img src="https://img.shields.io/badge/Plotly-%233F4F75.svg?style=for-the-badge&logo=plotly&logoColor=white"/>
<img src="https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white"/>
<img src="https://img.shields.io/badge/r-%23276DC3.svg?style=for-the-badge&logo=r&logoColor=white"/>
<div>
<div>
<img src="https://img.shields.io/badge/jupyter-%23FA0F00.svg?style=for-the-badge&logo=jupyter&logoColor=white"/>
<img src="https://img.shields.io/badge/pycharm-143?style=for-the-badge&logo=pycharm&logoColor=black&color=black&labelColor=green"/>
<img src="https://img.shields.io/badge/gitlab%20ci-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white"/>
<img src="https://img.shields.io/badge/Apache%20Airflow-017CEE?style=for-the-badge&logo=Apache%20Airflow&logoColor=white"/>
<img src="https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white"/>
<div>
<div>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Superset_logo.svg/320px-Superset_logo.svg.png"/>
<img src="https://upload.wikimedia.org/wikipedia/ru/thumb/0/06/Tableau_logo.svg/320px-Tableau_logo.svg.png"/>
<div>


### :envelope: Contact
In case you want to ask me questions, offer a cool project, share your ideas, check my [extended CV](https://drive.google.com/file/d/1vYGjOSYoLkQtkbvjP_WNpOFa79f8X50Q/view?usp=sharing) and message me in [Telegram](https://t.me/Trieg01).


