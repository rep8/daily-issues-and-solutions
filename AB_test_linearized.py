#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pandahouse
import datetime
import io
import numpy as np
import scipy.stats as stats


# In[2]:


connection = {
    'host': 'http://clickhouse.host',
    'password': 'dpo_22',
    'user': 'user',
    'database': 'simulator'
    }


q = """
SELECT exp_group, 
    user_id,
    sum(action = 'like') as likes,
    sum(action = 'view') as views,
    likes/views as ctr
FROM simulator_20220520.feed_actions 
WHERE toDate(time) between '2022-04-24' and '2022-04-30'
    and exp_group in (0,3)
GROUP BY exp_group, user_id
"""


df03 = pandahouse.read_clickhouse(q, connection=connection)


# In[3]:


connection = {
    'host': 'http://clickhouse.lab.karpov.courses',
    'password': 'dpo_python_2020',
    'user': 'student',
    'database': 'simulator'
    }


q = """
SELECT exp_group, 
    user_id,
    sum(action = 'like') as likes,
    sum(action = 'view') as views,
    likes/views as ctr
FROM simulator_20220520.feed_actions 
WHERE toDate(time) between '2022-04-24' and '2022-04-30'
    and exp_group in (1,2)
GROUP BY exp_group, user_id
"""


df12 = pandahouse.read_clickhouse(q, connection=connection)


# In[4]:


df03_CTRcontrol = sum(df03[df03.exp_group == 0].likes)/sum(df03[df03.exp_group == 0].views)


# In[10]:


stats.ttest_ind(df03[df03.exp_group == 0].ctr,
                df03[df03.exp_group == 3].ctr,
                equal_var=False)


# t-тест прокрасился

# In[5]:


df0_linearized = df03[df03.exp_group == 0].likes - df03_CTRcontrol*df03[df03.exp_group == 0].views
df3_linearized = df03[df03.exp_group == 3].likes - df03_CTRcontrol*df03[df03.exp_group == 3].views


# In[6]:


stats.ttest_ind(df0_linearized,
                df3_linearized,
                equal_var=False)


# Тест продемонстрировал очень хороший результат p-уровня значимости, гораздо лучшее значение, чем без линеаризации: значение статистики по модулю выше, а p-value ещё меньше

# In[7]:


df12_CTRcontrol = sum(df12[df12.exp_group == 1].likes)/sum(df12[df12.exp_group == 1].views)


# In[11]:


stats.ttest_ind(df12[df12.exp_group == 1].ctr,
                df12[df12.exp_group == 2].ctr,
                equal_var=False)


# Тест вообще не прокрасился

# In[8]:


df1_linearized = df12[df12.exp_group == 1].likes - df12_CTRcontrol*df12[df12.exp_group == 1].views
df2_linearized = df12[df12.exp_group == 2].likes - df12_CTRcontrol*df12[df12.exp_group == 2].views


# In[9]:


stats.ttest_ind(df1_linearized,
                df2_linearized,
                equal_var=False)


# А здесь тест уже продемонстрировал очень хороший результат p-уровня значимости

# In[ ]:




