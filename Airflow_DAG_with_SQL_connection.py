#!/usr/bin/env python
# coding: utf-8

# In[3]:


from datetime import datetime, timedelta
import pandas as pd
from io import StringIO
import requests

from airflow.decorators import dag, task
from airflow.operators.python import get_current_context

# Дефолтные параметры, которые прокидываются в таски
default_args = {
    'owner': 'a.novikov',
    'depends_on_past': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'start_date': datetime(2022, 6, 6)
}

# Интервал запуска DAG
schedule_interval = '0 10 * * *'

connection = {
    'host': 'https://clickhouse.hpost',
    'password': 'dpo_python',
    'user': 'student',
    'database': 'simulator'
}

connection2 = {
    'host': 'https://clickhouse.web',
    'password': 'pass',
    'user': 'student',
    'database': 'test'}

def query(q):
    df = pandahouse.read_clickhouse(q, connection=connection)
    return df

@dag(default_args=default_args, schedule_interval=schedule_interval, catchup=False)
def dag_novikov():

    @task()
    # экстракт нужных данных из feed
    def feed():
        q='''SELECT
        toDate(time) as event_date, 
        user_id,
        gender,
        age,
        os,
        countIf(action, action='like') AS likes,
        countIf(action, action='view') AS views
        FROM simulator_20220320.feed_actions  
        WHERE toDate(time)>=yesterday()
        GROUP BY event_date, user_id, gender, age, os'''
        
        df_feed = pandahouse.read_clickhouse(q, connection=connection)
        return df_feed
        
    @task()
    # экстракт нужных данных из messages
    def messages(): 
        q = '''SELECT 
            user_id,
            event_date,
            messages_sent,
            users_sent,
            messages_received,
            users_received
        FROM (
            SELECT
            user_id,
            toDate(time) AS event_date,
            count(reciever_id) AS messages_sent, --Число отправленных сообщений
            count(distinct reciever_id) AS users_sent --Количество разных пользователей которым отправили сообщение
            from simulator_20220420.message_actions
            WHERE event_date = yesterday()
            GROUP BY user_id, event_date
            ) t1
            JOIN
            (
            SELECT
            reciever_id,
            toDate(time) AS event_date,
            count(user_id) AS messages_received, --Число полученных сообщений
            count(distinct user_id) AS users_received -- Количество разных пользователей которые получили сообщения
            FROM simulator_20220420.message_actions
            WHERE event_date = yesterday()
            GROUP BY reciever_id, event_date
            )t2
            ON
            t1.user_id = t2.reciever_id'''
        
        df_messages = pandahouse.read_clickhouse(q, connection=connection)
        return df_messages
    
    @task()
    # Объединение датафреймов
    def merge(df_feed, df_messages): 
        df = pd.merge(df_feed, df_messages, on=['event_date','user_id','gender','age','os'],how = 'outer')
        return df
    
    @task
    #Срез по полу
    def df_gender(df):
        df_gender = df.groupby(['event_date','gender']).agg({'likes':'sum','views':'sum','messages_sent':'sum','users_sent':'sum',
                                    'messages_received':'sum',
                                    'users_received':'sum',
                                    }).reset_index()
        return df_gender
    
    @task
    #Срез по ОС
    def df_os(df):
        df_os = df.groupby(['event_date','os']).agg({'likes':'sum','views':'sum','messages_sent':'sum','users_sent':'sum',
                                    'messages_received':'sum',
                                    'users_received':'sum',
                                    }).reset_index()
        return df_os
    
    @task
    #TСрез по возрасту
    def df_age(df):
        df_age = df_full.groupby(['event_date','age']).agg({'likes':'sum','views':'sum','messages_sent':'sum','users_sent':'sum',
                                    'messages_received':'sum',
                                    'users_received':'sum',
                                            }).reset_index()
        return df_age
    
    @task
    #Объединение срезов
    def df_concat(df_gender, df_os, df_age):
        df_to_load = pd.concat([df_gender, df_os, df_age], ignore_index=True)
        return df_to_load
    
    @task
    #Загрузка
    def load(df_to_load):
        q = '''CREATE TABLE IF NOT EXISTS a.novikov
                (   event_date Date,
                    gender String,
                    age String,
                    os String,
                    views UInt64,
                    likes UInt64,
                    messages_received UInt64,
                    messages_sent UInt64,
                    users_received UInt64,
                    users_sent UInt64
                ) ENGINE = Log()'''

        pandahouse.execute(connection=connection2, query=q)
        pandahouse.to_clickhouse(df=df_to_load, table='a.novikov', index=False,                          connection = connection2)
       
    df_feed = feed()
    df_messages=messages()
    df=merge(df_feed, df_messages)
    df_gender = df_gender(df)
    df_os = df_os(df) 
    df_age = df_age(df)
    df_to_load = df_concat(df_gender, df_os, df_age)
    load(df_to_load)
dag_novikov = dag_novikov()


# In[ ]:




