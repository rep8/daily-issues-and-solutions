#!/usr/bin/env python
# coding: utf-8

# In[432]:


import telegram
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns 
sns.set() 
import io
import pandahouse
import os
import datetime
from datetime import datetime, timedelta


# In[ ]:


updates = bot.getUpdates() 
print(updates[-1])
# id имеется в строке id при просмотре последнего обновления бота


# In[430]:


def report_novikov(chat=None):
    
    chat_id = 2641888988989787979
    bot = telegram.Bot(token='bot_token')


    connection = {
        'host': 'http://clickhouse.host.address',
        'password': 'pass_2022',
        'user': 'my_name',
        'database': 'simulator'
    }

    # прописываем данные для коннекта
    msg_dau = "SELECT count(DISTINCT user_id) AS DAU FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > yesterday() AND time < today()"

    msg_dau_lastday = pandahouse.read_clickhouse(msg_dau, connection=connection)
    msg_dau_lastday = 'DAU is ' + str(msg_dau_lastday['DAU'].values[0])



    msg_likes = "SELECT count(user_id) AS LIKES FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > yesterday() AND time < today() AND action = 'like'" 

    msg_likes_lastday = pandahouse.read_clickhouse(msg_likes, connection=connection)
    msg_likes_lastday = 'Number of likes is ' + str(msg_likes_lastday['LIKES'].values[0])


    msg_views = "SELECT count(user_id) AS VIEWS FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > yesterday() AND time < today() AND action = 'view'" 

    msg_views_lastday = pandahouse.read_clickhouse(msg_views, connection=connection)
    msg_views_lastday = 'Number of views is ' + str(msg_views_lastday['VIEWS'].values[0])



    msg_CTR = "SELECT LIKES / VIEWS AS CTR FROM(SELECT COUNT(user_id) FILTER (WHERE action = 'like') AS LIKES, COUNT(user_id) FILTER (WHERE action = 'view') AS VIEWS FROM simulator_20220520.feed_actions AS virtual_table  WHERE virtual_table.time > yesterday() AND time < today())q1" 

    msg_CTR_lastday = pandahouse.read_clickhouse(msg_CTR, connection=connection)
    msg_CTR_lastday = 'CTR is ' + str(round(msg_CTR_lastday['CTR'].values[0], 2))

    msg = 'Report for ' + datetime.strftime(datetime.now() - timedelta(1), '%d-%m-%Y') + '\n' + msg_dau_lastday + '\n' + msg_likes_lastday + '\n' + msg_views_lastday + '\n' + msg_CTR_lastday

    bot.sendMessage(chat_id = chat_id, text = msg) 
    # отправка сообщения



    q = "SELECT toStartOfDay(toDateTime(time)) AS timestamp, count(DISTINCT user_id) AS DAU FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > today() - 7 AND time < today() GROUP BY toStartOfDay(toDateTime(time)) ORDER BY timestamp ASC LIMIT 10000;"

    DAU_7days = pandahouse.read_clickhouse(q, connection=connection)
    DAU_7days['timestamp'] = pd.to_datetime(DAU_7days['timestamp']).dt.strftime("%b-%d")



    q = "SELECT toStartOfDay(toDateTime(time)) AS timestamp, count(user_id) AS likes FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > today() - 7 AND time < today() AND action = 'like' GROUP BY toStartOfDay(toDateTime(time)) ORDER BY timestamp ASC LIMIT 10000;"

    likes_7days = pandahouse.read_clickhouse(q, connection=connection)
    likes_7days['timestamp'] = pd.to_datetime(likes_7days['timestamp']).dt.strftime("%b-%d")



    q = "SELECT toStartOfDay(toDateTime(time)) AS timestamp, count(user_id) AS views FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > today() - 7 AND time < today() AND action = 'view' GROUP BY toStartOfDay(toDateTime(time)) ORDER BY timestamp ASC LIMIT 10000;"

    views_7days = pandahouse.read_clickhouse(q, connection=connection)
    views_7days['timestamp'] = pd.to_datetime(views_7days['timestamp']).dt.strftime("%b-%d")



    q = "SELECT toStartOfDay(toDateTime(time)) AS timestamp, countIf(user_id, action = 'like')/countIf(user_id, action='view') AS CTR FROM (SELECT * FROM simulator_20220520.feed_actions) AS virtual_table WHERE time > today() - 7 AND time < today() GROUP BY toStartOfDay(toDateTime(time)) ORDER BY timestamp ASC LIMIT 10000;"

    CTR_7days = pandahouse.read_clickhouse(q, connection=connection)
    CTR_7days['timestamp'] = pd.to_datetime(CTR_7days['timestamp']).dt.strftime("%b-%d")

    # создадим графики
    fig, ax = plt.subplots(3,1,figsize=(10,10))


    sns.lineplot(x = 'timestamp', y = 'DAU', data = DAU_7days, ax = ax[0])
    ax[0].set_title('DAU last 7 days')
    ax[0].set_xlabel('')
    ax[0].set_ylabel('')

    sns.lineplot(x = 'timestamp', y = 'likes', data = likes_7days, ax = ax[1])
    sns.lineplot(x = 'timestamp', y = 'views', data = views_7days, ax = ax[1])
    ax[1].set_title('Likes and views last 7 days')
    ax[1].set_xlabel('')
    ax[1].set_ylabel('')

    sns.lineplot(x = 'timestamp', y = 'CTR', data = CTR_7days, ax = ax[2])
    ax[2].set_title('CTR last 7 days')
    ax[2].set_xlabel('')
    ax[2].set_ylabel('')


    fig.tight_layout()


    report_7days_plt = io.BytesIO()
    plt.savefig(report_7days_plt)
    report_7days_plt.name = 'report_7days_plt.png'
    report_7days_plt.seek(0)
    plt.close() 
    bot.sendPhoto(chat_id = chat_id, photo = report_7days_plt)
    # отправим графики



# In[431]:


try:
    report_novikov()
except Exception as e:
    print(e)


# In[ ]:


file_object = io.StringIO()
df.to_csv(file_object)
file_object.seek(0)
file_object.name = 'df'

bot.sendDocument(chat_id = chat_id, document = file_object)
# отправим документ

