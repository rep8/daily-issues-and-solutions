#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import telegram
import pandahouse
from datetime import date, timedelta
import datetime
import io
import sys
import os


# In[64]:


def check_anomaly(df, metric, a=4, n=4):
    # функция check_anomaly предлагает алгоритм проверки значения на аномальность посредством
    # сравнения интересующего значения со значением в это же время сутки назад
    # при желании алгоритм внутри этой функции можно изменить
    df['q25'] = df[metric].shift(1).rolling(n).quantile(0.25)
    # значение границы самого периода сдвигается на 1 период назад при помощи сдвига shift()
    df['q75'] = df[metric].shift(1).rolling(n).quantile(0.75)
    df['iqr'] = df['q75'] - df['q25']
    # назначим верхние и нижние границы относительно квантилей и межквантильного размаха, а также коэффициента a
    df['up'] = df['q75'] + a * df['iqr']
    df['low'] = df['q25'] - a * df['iqr']
    
    # сгладим границы скользящего среднего с окном n посередине, min_periods избавляет от границ на графике - NaNов
    df['up'] = df['up'].rolling(n, center=True, min_periods=1).mean()
    df['low'] = df['low'].rolling(n, center=True, min_periods=1).mean()
    
    # проверяем больше ли отклонение метрики заданного порога 
    # если отклонение последнего периода (15минутки) больше верхней границы или меньше нижней, кидаем алерт
    if df[metric].iloc[-1] < df['low'].iloc[-1] or df[metric].iloc[-1] > df['up'].iloc[-1]:
        is_alert = 1
    else:
        is_alert = 0

    return is_alert, df


# In[65]:


def run_alerts_feed(chat=None):
    chat_id = chat or 2641888982342
    bot = telegram.Bot(token='token_name')
    
    connection = {
    'host': 'http://clickhouse.host.connect',
    'password': 'pass_2022',
    'user': 'name',
    'database': 'simulator'
    }

    # для удобства построения графиков в запрос можно добавить колонки date, hm
    q = '''SELECT toStartOfFifteenMinutes(time) as ts, 
    toDate(ts) as date,
    formatDateTime(ts, '%R') as hm,
    uniqExact(user_id) as users_feed,
    countIf(user_id, action = 'view') AS views,
    countIf(user_id, action = 'like') AS likes,
    countIf(user_id, action = 'like')/countIf(user_id, action='view') AS CTR
    FROM simulator_20220520.feed_actions
    WHERE ts >=  today() - 1 and ts < toStartOfFifteenMinutes(now())
    GROUP BY ts, date, hm
    ORDER BY ts'''
    
    data = pandahouse.read_clickhouse(q, connection=connection)
    
    
    metrics_list = ['users_feed', 'views', 'likes', 'CTR'] 
    for metric in metrics_list:
        print(metric)
        df = data[['ts', 'date', 'hm', metric]].copy()
        is_alert, df = check_anomaly(df, metric) # проверяем метрику на нормальность алгоритмом, описаным внутри функции check_anomaly()
        if is_alert == 1 or True:
            msg = '''Метрика {metric} в точке {slice}: \n текущее значение: {current_value:.2f} \n отклонение от значения вчера {last_value_diff:.2%}'''.format(metric=metric, slice=df['hm'].iloc[-1],
                                                                                                                     current_value=df[metric].iloc[-1],
                                                                                                                     last_value_diff=abs(1 - (df[metric].iloc[-1]/ df[metric].iloc[-2])))
        
        
# задаем размер графика
            sns.set(rc={'figure.figsize': (16, 10)})
            plt.tight_layout()

            # строим линейный график
            ax = sns.lineplot(x = df['ts'], y = df[metric], label=metric)
            ax = sns.lineplot(x = df['ts'], y = df['up'], label='upper bound') 
            ax = sns.lineplot(x = df['ts'], y = df['low'], label='lower bound') 

            for ind, label in enumerate(ax.get_xticklabels()): # этот цикл нужен чтобы разрядить подписи координат по оси Х,
                if ind % 2 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
                    
            ax.set(xlabel='Day-Month-Hour') # задаем имя оси Х
            ax.set(ylabel=metric) # задаем имя оси У

            ax.set_title('{}'.format(metric)) # задаем заголовок графика
            ax.set(ylim=(0, None)) # задаем нижний лимит для оси У

            # формируем файловый объект
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = '{0}.png'.format(metric)
            plt.close()

            # отправляем алерт
            bot.sendMessage(chat_id=chat_id, text=msg)
            bot.sendPhoto(chat_id=chat_id, photo=plot_object)

    return


# In[66]:


try:
    run_alerts_feed()
except Exception as e:
    print(e)


# In[ ]:




